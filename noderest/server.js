var express = require('express')
var app = express()

port =process.env.PORT || 5000
mongoose = require('mongoose')

User = require('./api/model/userListModel')
bodyParser = require('body-parser')

mongoose.Promise = global.Promise
mongoose.connect('mongodb+srv://admin:123@cluster0.4tytg.mongodb.net/ContactList?retryWrites=true&w=majority', function(error){
    if(error) throw error
    console.log('Successfully connected')
})

const cors = require('cors')
app.use(cors())

app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

var routes = require('./api/routes/userListRoutes')
routes(app)
app.listen(port)
console.log('User List Server started on : '+port)