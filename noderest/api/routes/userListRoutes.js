'use strict'

module.exports = function(app){
    var userList = require('../controllers/userListController')

    app.route('/contacts')
    .get(userList.listAllUsers)
    .post(userList.createAUser)

    app.route('/contacts/edit/:userId')
    .post(userList.updateAUser)

    app.route('/contacts/read/:userId')
    .get(userList.readAUser)

    app.route('/contacts/delete/:userId')
    .delete(userList.deleteAUser)




}