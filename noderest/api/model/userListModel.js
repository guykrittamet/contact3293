'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var UserSchema = new Schema({
    contactID: {
        type : String,
        Required : 'Please enter'
    },
    firstName: {
        type : String,
        Required : 'Please enter'
    },
    lastName: {
        type : String,
        Required : 'Please enter'
    },
    mobileNo: {
        type : String,
        Required : 'Please enter'
    },
    email: {
        type : String,
        Required : 'Please enter'
    },
    facebook: {
        type : String,
        Required : 'Please enter'
    },
    imageUrl: {
        type : String,
        Required : 'Please enter'
    }
})

module.exports = mongoose.model('Contacts', UserSchema) //connect to collection users