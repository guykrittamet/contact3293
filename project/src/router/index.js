import { createRouter, createWebHistory } from 'vue-router'

import AddContact from '../components/AddContract.vue'
import EditContact from '../components/EditContract.vue'
import SignIn from '../components/SignIn.vue'
import ContactList from '../components/Contactlist.vue'

const routerHistory = createWebHistory()

const routes = [

    {
        path: '/',
        redirect: '/signin'
    },
    {
        path: '/:catchAll(.*)',
        redirect: '/signin'
    },
    {
        path: '/signin',
        name: SignIn,
        component: SignIn
    },
    {
        path: '/addcontact',
        name: AddContact,
        component: AddContact
    },
    {
        path: '/editcontact/:userId',
        name: 'EditContact',
        component: EditContact
    },
    {
        path: '/contactlists',
        name: 'Contactlist',
        component: ContactList
    },



    
]

const router = createRouter(
    {
        history: routerHistory,
        routes
    }
)


export default router